# Teorias da história da arquitetura no ensino profissional: (anti)disciplinaridade e crítica

### Resumo ###

O ensino de história no âmbito da formação profissional em arquitetura,
tendo afastado os debates do pós-modernismo e da « virada linguística »,
assentou-se sem alarde num espelho do ofício do docente investigador. As
unidades de história da arquitetura são hoje vistas tanto como
propedêutica à formação de futuros investigadores quanto num espírito de
formação crítica não disciplinar. Tal perspetiva implicou o abandono,
mas não a superação, do debate sobre a pertinência da história enquanto
parte do campo disciplinar da arquitetura. Neste ensejo, esta
conferência se propõe a explicitar os termos nos quais se poderá debater
a especificidade da história na formação de profissionais do projeto
arquitectónico.

Para tanto, abordamos a delimitação do objeto de estudo da história da
arquitetura: ora este objeto é a evolução das relações de produção e das
ideologias, resultando num viés antidisciplinar e na compartimentação de
períodos históricos; ora o objeto será o palimpsesto do ambiente
construído acumulado ao longo do tempo, universo disciplinar por
excelência da arqueologia e dos estudos morfológicos, amiúde cedidos às
unidades didáticas de projeto.

Por fim, exploramos três modos de apropriação do ambiente construído e
suas implicações para a formação profissional: a arquitetura comparada,
que moldou o formato canónico do livro-texto de história da arquitetura
durante quase todo o século XX; a arquitetura analítica, hoje quase
esquecida no âmbito da história mas ainda popular nos livros-texto de
sistemas construtivos; e a arquitetura histórica, comprometida com a
crítica das culturas e ideologias autoconscientes do seu próprio devir
histórico.

## Theories of architectural history in professional education: (anti)disciplinary biases and criticism

### Abstract ###

History teaching in the professional education of architects, having
sidelined the debates of postmodernism and the "linguistic turn,"
settled quietly into a reproduction of the research-focused professor's
craft. Architectural history courses are thus now seen both as a
preparation for the specialization of future scholars, and as an
introduction to non-disciplinary criticism. Such a perspective entailed
suppressing, but not overcoming, the debate on the place of history
within the disciplinary field of architecture. This conference aims
therefore to bring to the fore the discussion about the specific
character history may take on in the education of professional
designers.

To this end, I address the definition of architectural history's subject
matter: this may alternatively be the evolution of relations of
production and of ideologies, which implies an anti-disciplinary bias
and chronological segmentation; or it may be the palimpsest of the built
environment built up over time, a subject of disciplinary expertise in
archaeology and morphology often handed over to design studio courses.

Finally, I explore three modes of appropriation of the built environment
and their implications for professional education: comparative
architecture, which set up the canonical textbook format of the
twentieth century; analytical architecture, now almost forgotten among
historians but still popular in construction systems textbooks; and
historical architecture, unapologetically dedicated to the critique of
cultures and ideologies that are self-conscious of their own
historicity.

### Nota biográfica ###

Arquiteto e historiador da arquitetura com experiência em preservação e
gestão de sítios culturais. Investigo as tradições construtivas do
Mediterrâneo ocidental e do mundo lusófono nos séculos XVIII e XIX, com
ênfase nas interfaces entre a construção tradicional e a moderna, bem
como entre a arquitetura erudita da tradição clássica e as tipologias
vernaculares.

Professor associado na Faculdade de Arquitetura e Urbanismo da
Universidade de Brasília e investigador visitante junto ao Centro de
Estudos Sociais da Universidade de Coimbra. Membro do Comité de
documentação do conselho científico do Icomos Brasil.

### Biographical note ###

Architectural historian and a historic preservationist specializing in
the western Mediterranean and Portuguese-speaking world in the
eighteenth and nineteenth centuries. My research explores the interfaces
between traditional and modern construction as well as between the
learned architecture of classicism and vernacular typologies.

Currently associate professor at the University of Brasilia School of
Architecture and Urbanism and a visiting scholar at the University of
Coimbra Center for Social Studies. Member of the Documentation committee
of Icomos Brazil.

## License ##

teoria-historia-arq-coimbra2023 (c) 2023 by Pedro P. Palazzo.

![](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png)

teoria-historia-arq-coimbra2023 is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International

You should have received a copy of the license along with this work. If
not, see http://creativecommons.org/licenses/by-sa/4.0/.

